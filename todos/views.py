from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm




def show_todo_lists(request):
    todolists = TodoList.objects.all()
    context = {
        "todolist_list": todolists,
    }
    return render(request, "todo_lists/list.html", context)

def show_todo_list(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    context = {
        "todolist": todolist,
    }
    return render(request, "todo_lists/detail.html", context)

def create_todo_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todolist = form.save()
            return redirect("todo_list_detail", todolist.id)
    else:
        form = TodoListForm()
    context = {
        "form": form
    }
    return render(request, "todo_lists/create.html", context)


def update_todo_list(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            todolist = form.save()
            return redirect("todo_list_detail", todolist.id)
    else:
        todolist = get_object_or_404(TodoList, id=id)
        form = TodoListForm(instance=todolist)
    context = {
        "form": form
    }
    return render(request, "todo_lists/update.html", context)


def delete_todo_list(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list_list")
    return render(request, "todo_lists/delete.html")


def create_todo_item(request):
    if request.method == "POST":
        form=TodoItemForm(request.POST)
        if form.is_valid():
            todoitem = form.save()
            return redirect("todo_list_detail", todoitem.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todo_items/create.html", context)

def update_todo_item(request, id):
    todoitem = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todoitem)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", todoitem.list.id)
    else:
        form = TodoItemForm(instance=todoitem)
    context = {
        "form":form,
    }
    return render(request, "todo_items/update.html", context)

from django.urls import path
from todos.views import show_todo_lists, show_todo_list, create_todo_list, update_todo_list, delete_todo_list, create_todo_item, update_todo_item



urlpatterns = [
    path("items/<int:id>/edit/", update_todo_item, name="todo_item_update"),
    path("items/create/", create_todo_item, name="todo_item_create"),
    path("<int:id>/delete/", delete_todo_list, name="todo_list_delete"),
    path("<int:id>/edit/", update_todo_list, name="todo_list_update"),
    path("create/", create_todo_list, name="todo_list_create"),
    path("<int:id>/", show_todo_list, name="todo_list_detail"),
    path("", show_todo_lists, name="todo_list_list"),
]
